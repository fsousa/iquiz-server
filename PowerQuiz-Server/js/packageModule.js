function createHttpObject() {
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	return xmlhttp;
	
}

function searchModule(pattern) {
	xmlhttp = this.createHttpObject();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("searchResult").innerHTML = xmlhttp.responseText;
			$(window).resize();
		}
	};
	xmlhttp.open("GET", "module/searchModuleAssync.php?q=" + pattern, true);
	xmlhttp.send();
	
}

function showPage(page) {
	xmlhttp = this.createHttpObject();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
			$(window).resize();
		}
	};
	xmlhttp.open("GET", page, true);
	xmlhttp.send();
}

///////////////////////////////////////
///////////////////////// Module /////
/////////////////////////////////////

function submitFormModuleDelete() {
	var r=confirm("Are you sure ?");
    if (r==true)
      {
    	$.ajax({type:'POST', url: 'module/reciveDeleteForm.php', data:$('#editmodule').serialize(), success: function(response) {
            alert("Module Delete Successfully");
            showPage("module/show.php");
        }});
      }
    return false;
}

function submitModulePublish() {
	$.ajax({type:'POST', url: 'module/recivePublishModule.php', data:$('#publishModule').serialize(), success: function(response) {
		if(response ==  1) {
			alert("Module Published");
		}else {
			alert("Erro: Module not published");
		}
        
        showPage("module/show.php");
    }});
    return false;
}

function submitModuleUnpublish() {
	$.ajax({type:'POST', url: 'module/reciveUnpublishModule.php', data:$('#publishModule').serialize(), success: function(response) {
		if(response ==  1) {
			alert("Module Unpublished");
		}else {
			alert("Erro: Module not Unpublished");
		}
        
		 showPage("module/show.php");
    }});
    return false;
}
/////////////////////////////////////
///////////////////////// QUIZ /////
///////////////////////////////////


function submitFormQuizDelete() {
	var r=confirm("Are you sure ?");
    if (r==true)
      {
    	$.ajax({type:'POST', url: 'quiz/reciveDeleteQuiz.php', data:$('#editquestion').serialize(), success: function(response) {
    		if(response == true) {
    			alert("Quiz Delete Successfully");
    		}else {
    			alert("Erro: Quiz not deleted");
    		}
            showPage("module/details.php?id="+$('input[name=mod_id]').val());
        }});
      }
    return false;
}

function submitFormAlternativeDelete() {
	var r=confirm("Are you sure ?");
    if (r==true)
      {
    	$.ajax({type:'POST', url: 'quiz/reciveDeleteAlternative.php', data:$('#EditAlternative').serialize(), success: function(response) {
    		if(response == true) {
    			alert("Quiz Delete Successfully");
    		}else {
    			alert("Erro: Quiz not deleted");
    		}
            showPage("quiz/details.php?qid="+$('input[name=quiz_id]').val());
        }});
      }
    return false;
}

/////////////////////////////////////
///////////////////////// USER /////
///////////////////////////////////

function logout() {
	xmlhttp = this.createHttpObject();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			window.location = "/iquiz/PowerQuiz-Server/view/";
		}
	};
	xmlhttp.open("GET", "user/reciveLogoutForm.php", true);
	xmlhttp.send();
}

function submitFormUserLogin() {
    $.ajax({type:'POST', url: 'user/reciveLoginForm.php', data:$('#form-signin').serialize(), success: function(response) {
    	if(response == "True") {
    		window.location = "/iquiz/PowerQuiz-Server/view/";
    	}else {
    		$('#form-signin').find('.MyResult').html(response).fadeIn(1000).fadeOut(5000);
    	}
    }});
    return false;
}

function deleteAccount() {
	var r=confirm("Are you sure ?");
	if (r==true) {
		xmlhttp = this.createHttpObject();
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				if(xmlhttp.responseText == "True") {
					alert("Your account was deleted.");
					logout();
				}else {
					alert("Something Went Wrong");
				}
			}
		};
		xmlhttp.open("POST", "user/reciveDeleteProfile.php", true);
		xmlhttp.send();
	}
}






