$(document).ready(function(){
    jQuery.validator.addMethod("categoryValid", function(value, element) {
    	if ($('#categorys').val() == '') {  
            return false;  
        } else {  
            return true;  
        }  
    }
, "Please, select a category");
});

function validCreateModule() {
	$("#createmodule").validate({
		submitHandler: function(form) {
			//Submit Form
			$.ajax({type:'POST', url: 'module/reciveCreateForm.php', data:$('#createmodule').serialize(), success: function(response) {
		        $('#createmodule').find('.MyResult').html(response).fadeIn(1000).fadeOut(5000);
		        $('#createmodule')[0].reset();
		    }});
		},
		rules : {
			name : {
				required : true,
				minlength : 5,
				remote: {
			        url: "module/checkUniqueModuleName.php",
			        type: "post",
			        data: {
			        	name: function() {
			              return $("#name").val();
			            }
			          }
				}
			},
			textArea : {
				required : true,
				minlength : 10
			},
			price : {
				required : true,
				min : 0
			},
			categorys : {
				categoryValid : true
			}
		},
		messages : {
			name : {
				required : "Name is required",
				minLength : "O seu nome deve conter, no m�nimo, 5 caracteres",
				remote : "Module Name already exists"
			},
			textArea : {
				required : "Description is required",
				minLength : "O seu nome deve conter, no m�nimo, 10 caracteres"
			},
			price : {
				required : "Price is required",
				min : "Min value is 0"
			}
		}
	});
}

function validEditModule() {
	$("#editmodule").validate({
		submitHandler: function(form) {
			//Submit Form
			$.ajax({type:'POST', url: 'module/reciveEditForm.php', data:$('#editmodule').serialize(), success: function(response) {
		        $('#editmodule').find('.MyResult').html(response).fadeIn(1000).fadeOut(5000);
		    }});
		},
		rules : {
			name : {
				required : true,
				minlength : 5,
				remote: {
			        url: "module/checkEditExistsModule.php",
			        type: "post",
			        data: {
			        	name: function() {
			              return $("#name").val();
			            },
			            currentName : function() {
			            	return $("#currentName").val();
			            }
			          }
				}
			},
			textArea : {
				required : true,
				minlength : 10
			},
			price : {
				required : true,
				min : 0
			},
			categorys : {
				categoryValid : true
			}
		},
		messages : {
			name : {
				required : "Name is required",
				minLength : "O seu nome deve conter, no m�nimo, 5 caracteres",
				remote : "Module Name already exists"
			},
			textArea : {
				required : "Description is required",
				minLength : "O seu nome deve conter, no m�nimo, 10 caracteres"
			},
			price : {
				required : "Price is required",
				min : "Min value is 0"
			}
		}
	});
}


function validCreateQuestion() {
	$("#createquiz").validate({
		submitHandler: function(form) {
			//Submit Form
			$.ajax({type:'POST', url: 'quiz/reciveCreateQuiz.php', data:$('#createquiz').serialize(), success: function(response) {
		        $('#createquiz').find('.MyResult').html(response).fadeIn(1000).fadeOut(5000);
		        $('#createquiz')[0].reset();
		    }});
		},
		rules : {
			question : {
				required : true,
				minlength : 5,
				remote: {
			        url: "quiz/checkUniqueQuizName.php",
			        type: "post",
			        data: {
			        	question: function() {
			              return $("#question").val();
			            },
			            mod_id : function() {
			            	return $("#mod_id").val();
			            }
			          }
				}
			}
		},
		messages : {
			question : {
				required : "Question is required",
				minLength : "Your question have to be at least 5 characters",
				remote : "Question already exists on your module"
			}
		}
	});
}

function validEditQuestion() {
	$("#editquestion").validate({
		submitHandler: function(form) {
			//Submit Form
			$.ajax({type:'POST', url: 'quiz/reciveEditQuiz.php', data:$('#editquestion').serialize(), success: function(response) {
		        $('#editquestion').find('.MyResult').html(response).fadeIn(1000).fadeOut(5000);
		        
		    }});
		},
		rules : {
			question : {
				required : true,
				minlength : 5,
				remote: {
			        url: "quiz/checkUniqueEditQuiz.php",
			        type: "post",
			        data: {
			        	question: function() {
			              return $("#question").val();
			            },
			            mod_id : function() {
			            	return $("#mod_id").val();
			            },
			            currentQuestion: function() {
				              return $("#currentQuestion").val();
				            }
			          }
				}
			}
		},
		messages : {
			question : {
				required : "Question is required",
				minLength : "Your question have to be at least 5 characters",
				remote : "Question already exists on your module"
			}
		}
	});
}

function validCreateAlternative() {
	$("#createAlternative").validate({
		submitHandler: function(form) {
			//Submit Form
			$.ajax({type:'POST', url: 'quiz/reciveCreateAlternative.php', data:$('#createAlternative').serialize(), success: function(response) {
		        $('#createAlternative').find('.MyResult').html(response).fadeIn(1000).fadeOut(5000);
		        
		    }});
		},
		rules : {
			alternative : {
				required : true,
				minlength : 5,
				remote: {
			        url: "quiz/checkUniqueAlternativeQuiz.php",
			        type: "post",
			        data: {
			        	quiz_id: function() {
			              return $("#quiz_id").val();
			            },
			            alternative : function() {
			            	return $("#alternative").val();
			            }
			          }
				}
			}
		},
		messages : {
			alternative : {
				required : "Alternative is required",
				minLength : "Your Alternative have to be at least 5 characters",
				remote : "Alternative already exists on your Quiz"
			}
		}
	});
}

function validEditAlternative() {
	$("#EditAlternative").validate({
		submitHandler: function(form) {
			//Submit Form
			$.ajax({type:'POST', url: 'quiz/reciveEditAlternative.php', data:$('#EditAlternative').serialize(), success: function(response) {
		        $('#EditAlternative').find('.MyResult').html(response).fadeIn(1000).fadeOut(5000);
		        
		    }});
		},
		rules : {
			alternative : {
				required : true,
				minlength : 5,
				remote: {
			        url: "quiz/checkUniqueEditAlternative.php",
			        type: "post",
			        data: {
			        	quiz_id: function() {
			              return $("#quiz_id").val();
			            },
			            alternative : function() {
			            	return $("#alternative").val();
			            },
			            currentAlternative : function() {
			            	return $("#currentAlternative").val();
			            }
			        }
				}
			}
		},
		messages : {
			alternative : {
				required : "Alternative is required",
				minLength : "Your Alternative have to be at least 5 characters",
				remote : "Alternative already exists on your Quiz"
			}
		}
	});
}

function validUserRegister() {
	$("#form-signup").validate({
		submitHandler: function(form) {
			//Submit Form
			$.ajax({type:'POST', url: 'user/reciveRegisterForm.php', data:$('#form-signup').serialize(), success: function(response) {
				if(response == "True") {
		    		alert("User Create Successfully");
		    		window.location = "/iquiz/PowerQuiz-Server/view/";
		    	}else {
		    		$('#form-signup').find('.MyResult').html(response).fadeIn(1000).fadeOut(5000);
		    	}
		        
		    }});
		},
		rules : {
			name : {
				required : true,
				minlength : 3
			},
			last_name : {
				required : true,
				minlength : 3
			},
			email : {
				required : true,
				email : true,
				remote: {
			        url: "user/checkUniqueEmail.php",
			        type: "post",
			        data: {
			        	email : function() {
			              return $("#email").val();
			            }
			        }
				}
			},
			pass : {
				required : true,
				minlength : 6
			},
			pass2 : {
				required : true,
				equalTo: "#pass"
			}
			
		},
		messages : {
			name : {
				required : "Name is required",
				minLength : "Your name have to be at least 3 characters"
			},
			last_name : {
				required : "Last Name is required",
				minlength : "Your last name have to be at least 3 characters"
			},
			email : {
				required : "Email is required",
				email : "Please, enter a valid email address",
				remote : "Email already exists on database"
			},
			pass: {
				required : "Password is required",
				minlength : "Your password have to be at least 6 characters"
			},
			pass2: {
				required : "Password again is required",
				equalTo : "Your password dont match"
			}
		}
	});

}

function validateUpdateProfile() {
	$("#editProfileForm").validate({
		submitHandler: function(form) {
			$(form).ajaxSubmit({type:'POST', 
					url: 'user/reciveEditProfile.php', 
					data:$('#editProfileForm').serialize(), 
					success: function(response) {
						$('#editProfileForm').find('.MyResult').html(response).fadeIn(1000).fadeOut(5000);
		        
		    }});
		},
		rules : {
			name : {
				required : true,
				minlength : 3
			},
			last_name : {
				required : true,
				minlength : 3
			}
		},
		messages : {
			name : {
				required : "First Name is required",
				minLength : "Your First Name have to be at least 3 characters"
			},
			last_name : {
				required : "Last Name is required",
				minLength : "Your Last Name have to be at least 3 characters"
			}
		}
	});
}
