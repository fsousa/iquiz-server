<?php

class Module {
	
	private $mod_id;
	private $name;
	private $description;
	private $author_id;
	private $category_id;
	private $price;
	private $publish;
	
	public function __construct($mod_id, $name, $description, $author_id, $category_id, $price, $publish ) {
		$this->mod_id =  $mod_id;
		$this->name = $name;
		$this->description = $description;
		$this->author_id = $author_id;
		$this->category_id = $category_id;
		$this->price = $price;
		$this->publish =$publish;
	}
	
	public function __destruct() {
		unset($this->mod_id);
		unset($this->name);
		unset($this->description);
		unset($this->author_id);
		unset($this->category_id);
		unset($this->price);
		unset($this->publish);
	}
	
	public function getModID() {
		return $this->mod_id;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function getDescription() {
		return $this->description;
	}
	
	public function getAuthorId() {
		return $this->author_id;
	} 
	
	public function getCategoryId() {
		return $this->category_id;
	}
	
	public function getPrice() {
		return $this->price;
	}
	
	public function getPublish() {
		return $this->publish;
	}
	
	public function setModID($mod_id) {
		 $this->mod_id = $mod_id;
	}
	
	public function setName($name) {
		 $this->name = $name;
	}
	
	public function setDescription($description) {
		 $this->description = $description;
	}
	
	public function setAuthorId($author_id) {
		 $this->author_id = $author_id;
	}
	
	public function setCategoryId($category_id) {
		 $this->category_id = $category_id;
	}
	
	public function setPrice($price) {
		 $this->price = $price;
	}
	
	public function setPublish($publish) {
		 $this->publish = $publish;
	}
}

?>