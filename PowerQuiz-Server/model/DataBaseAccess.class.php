<?php

class DataBaseAccess
{
	///// DATABASE CONFIGURATION /////
	
	//DEV
	private $server = "localhost";
	private $user = 'root';
	private $pass = 'c0baia';
	private $db = 'iquiz-dev';

	
/*	//PRODUCE
	private $server = "localhost";
	private $user = 'root';
	private $pass = 'c0baianorceb';
	private $db = 'iquiz-dev';
*/

	static private $instance;

	private function __contructor() {

	}

	public function __clone()
	{
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}

	static public function getInstance() {
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}

		return self::$instance;
	}
	
	private function openConnection() {
		$mysqli = new mysqli($this->server, $this->user, $this->pass, $this->db);
		return $mysqli;
	}
	
	private function closeConnection($db) {
		$db->close();
	}

	private function runQuery($sql) {

		$mysqli = new mysqli($this->server, $this->user, $this->pass, $this->db);

		if (mysqli_connect_errno()) trigger_error(mysqli_connect_error());
		$mysqli->set_charset("iso-8859-1");
		$result = $mysqli->query($sql) or trigger_error($mysqli->error."[$sql]");
		$mysqli->close();
		return $result;

	}
	///MODULE ///
	public function listAllModules() {
		$sql = "SELECT m.mod_id as ID, m.name as NAME, m.description as DESCRIPTION, u.name AS AUTHOR_NAME, c.name as CATEGORY, m.price as PRICE, m.publish as PUBLISH
				FROM module m, user u, category c
				WHERE `author_id` = `user_id` AND `category_id` = `cat_id` ORDER BY m.mod_id;";
		$result = $this->runQuery($sql);
	
		return $result;
	}
	
	public function listAllModuleByUser($email) {
		$sql = "SELECT m.mod_id as ID, m.name as NAME, m.description as DESCRIPTION, u.name AS AUTHOR_NAME, c.name as CATEGORY, m.price as PRICE, m.publish as PUBLISH
		FROM module m, user u, category c
		WHERE `author_id` = `user_id` AND `category_id` = `cat_id` AND email = '$email' ORDER BY m.mod_id;";
		$result = $this->runQuery($sql);
	
		return $result;
	}
	
	public function listAllModuleBuyByUser($email) {
		$sql = "SELECT m.mod_id as ID, m.name as NAME, m.description as DESCRIPTION, u.name AS AUTHOR_NAME, c.name as CATEGORY, m.price as PRICE, m.publish as PUBLISH
				FROM module m, user u, category c, module_historical mh
				WHERE m.author_id = u.user_id AND category_id = cat_id AND email = 'felipe.sousa.f.s.s@gmail.com' AND mh.user_id = u.user_id AND mh.modh_id = m.mod_id AND m.publish = 1 ORDER BY m.mod_id;";
		$result = $this->runQuery($sql);
	
		return $result;
	}
	
	public function listModuleByID($id) {
		$sql = "SELECT m.mod_id as ID, m.name as NAME, m.description as DESCRIPTION, u.name AS AUTHOR_NAME, c.name as CATEGORY, m.price as PRICE, m.publish as PUBLISH
		FROM module m, user u, category c
		WHERE `author_id` = `user_id` AND `category_id` = `cat_id` and mod_id = '$id';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function listModuleByName($name) {
		$sql = "SELECT m.mod_id as ID, m.name as NAME, m.description as DESCRIPTION, u.name AS AUTHOR_NAME, c.name as CATEGORY, m.price as PRICE, m.publish as PUBLISH
		FROM module m, user u, category c
		WHERE `author_id` = `user_id` AND `category_id` = `cat_id` and m.name = '$name';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function listModuleByCategory($category) {
		$sql = "SELECT m.mod_id as ID, m.name as NAME, m.description as DESCRIPTION, u.name AS AUTHOR_NAME, c.name as CATEGORY, m.price as PRICECE, m.publish as PUBLISH
		FROM module m, user u, category c
		WHERE `author_id` = `user_id` AND `category_id` = `cat_id` AND c.name = '$category';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function insertModule($name, $description, $author_id, $category_id, $price) {
		$result = false;
		$conn = $this->openConnection();
		$pstmt = $conn->prepare("INSERT INTO module (name, description, author_id, category_id, price)
				VALUES (?, ?, ?, ?, ?)");
		$pstmt->bind_param('ssiid',$name, $description, $author_id, $category_id, $price);
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function deleteModule($mod_id) {
		$result = false;
		$conn = $this->openConnection();
		$pstmt = $conn->prepare("DELETE FROM module WHERE mod_id = ?");
		$pstmt->bind_param('i',$mod_id);
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function updateModule($id, $name, $description, $category_id, $price) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("UPDATE module SET
				name = ?, description = ?, category_id = ?, price = ?
				WHERE mod_id = ?");
		$pstmt->bind_param('ssidi',$name, $description, $category_id, $price, $id);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function listAllCategory() {
		$sql = "SELECT * FROM category;";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function retriveModuleItem($pattern) {
		$sql = "SELECT m.mod_id as ID, m.name as NAME, m.description as DESCRIPTION, u.name AS AUTHOR_NAME, c.name as CATEGORY, m.price as PRICE, m.publish as PUBLISH
		FROM module m, user u, category c
		WHERE `author_id` = `user_id` AND `category_id` = `cat_id` AND m.name LIKE '%$pattern%';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function addModuleToUser($mod_id, $user_id) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("INSERT INTO module_historical VALUES(?, ?)");
		$pstmt->bind_param('ii', $user_id, $mod_id);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	private function publishUnpublish($mod_id, $type) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("UPDATE module SET publish = ?
				WHERE mod_id = ?");
		$pstmt->bind_param('ii', $type, $mod_id);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function setPublishModule($mod_id) {
		return $this->publishUnpublish($mod_id, 1);
	}
	
	public function setUnpublishModule($mod_id) {
		return $this->publishUnpublish($mod_id, 0);
	}
	
	/// QUESTION ///
	public function listAllQuestionByModule($mod_id) {
		$sql= "SELECT * FROM quiz WHERE mod_id = '$mod_id';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function listItemQuestionByModule($mod_id, $quiz_id) {
		$sql= "SELECT * FROM quiz WHERE mod_id = '$mod_id' AND quiz_id = '$quiz_id';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function listQuizID($mod_id, $question) {
		$sql= "SELECT quiz_id, question FROM quiz WHERE mod_id = '$mod_id' AND question = '$question';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function listQuizById($quiz_id) {
		$sql= "SELECT * FROM quiz WHERE quiz_id = '$quiz_id';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function insertQuestionOnModule($mod_id, $question) {
		$result = false;
		$conn = $this->openConnection();
		$pstmt = $conn->prepare("INSERT INTO quiz (mod_id, question) VALUES(?, ?)" );
		$pstmt->bind_param('is',$mod_id, $question);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function removeQuestionOnModule($quiz_id) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("DELETE FROM quiz
				WHERE quiz_id = ?");
		$pstmt->bind_param('i',$quiz_id);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function deleteQuestionAlternative($alternative_id) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("DELETE FROM quiz_alternative
				WHERE alternative_id = ?");
		$pstmt->bind_param('i',$alternative_id);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function updateQuestionOnModule($quiz_id, $question) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("UPDATE quiz
				SET question = ? WHERE quiz_id = ?");
		$pstmt->bind_param('ss',$question, $quiz_id);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function updateQuestionAlternative($alternative_id, $alternative, $correct) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("UPDATE quiz_alternative
				SET alternative = ?, correct = ? WHERE alternative_id = ?");
		$pstmt->bind_param('sss',$alternative, $correct, $alternative_id);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function listAlternativesByQuestion($quiz_id) {
		$sql= "SELECT * FROM quiz_alternative WHERE quiz_id = '$quiz_id';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function listAlternativeByQuestionAndId($quiz_id, $alternative) {
		$sql= "SELECT * FROM quiz_alternative WHERE quiz_id = '$quiz_id' AND alternative = '$alternative';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function insertQuizAlternative($quiz_id, $alternative, $correct) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("INSERT INTO quiz_alternative (quiz_id, alternative, correct) VALUES (?, ?, ?)");
		$pstmt->bind_param('isi',$quiz_id, $alternative, $correct);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function listItemAlternativeByID($alternative_id) {
		$sql= "SELECT * FROM quiz_alternative WHERE alternative_id = '$alternative_id';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	/// USER ///
	public function loginUser($email, $pass) {
		$sql= "SELECT user_id, email, name, last_name, image FROM user WHERE email = '$email' AND password = '$pass';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function userDataByEmail($email) {
		$sql= "SELECT user_id, email, name, last_name, image FROM user WHERE email = '$email';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function registerUser($name, $lastName, $email, $password) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("INSERT INTO user (name, last_name, email, password) VALUES (?, ?, ?, ?)");
		$pstmt->bind_param('ssss',$name, $lastName, $email,$password);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	public function registerUserGoogle($name, $lastName, $email, $password) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("INSERT INTO user (name, last_name, email, password) VALUES (?, ?, ?, ?)");
		$pstmt->bind_param('ssss',$name, $lastName, $email,$password);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function	listUserDetails($email) {
		$sql= "SELECT user_id FROM user WHERE email = '$email';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function updateUser($name, $lastName, $email) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("UPDATE user SET name = ?, last_name = ? WHERE email = ?");
		$pstmt->bind_param('sss',$name, $lastName, $email);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function updateUserPhoto($email, $photo) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("UPDATE user SET image = ? WHERE email = ?");
		$pstmt->bind_param('ss', $photo, $email);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function removeUser($email) {
		$result = false;
		$conn = $this->openConnection();
		
		$pstmt = $conn->prepare("DELETE FROM user WHERE email = ?");
		$pstmt->bind_param('s', $email);
		
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}

}

?>