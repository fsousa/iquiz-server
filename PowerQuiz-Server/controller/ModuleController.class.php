<?php
require_once  $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/model/DataBaseAccess.class.php";

class ModuleController {
	
	static private $instance;
	
	private function __construct()  {
		
	}
	
	public function __clone() {
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}
	
	static public function getInstance() {
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}
		
		return self::$instance;
	}
	
	private function fetchArray($mysqlObject) {
		$rows = array();
		while($r = $mysqlObject->fetch_array(MYSQL_ASSOC)) {
			$rows[] = $r;
		}
		return $rows;
	}
	
	public function getAllModules() {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listAllModules();
		return $this->fetchArray($result);
	}
	
	public function getAllModulesByUser($email) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listAllModuleByUser($email);
		return $this->fetchArray($result);
	}
	
	public function getAllModulesBuyByUser($email) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listAllModuleBuyByUser($email);
		return $this->fetchArray($result);
	}
	 
	public function getModuleByID($id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listModuleByID($id);
		return $result;
	}
	
	public function getAllModulesByCategory($category) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listModuleByCategory($category);
		return $this->fetchArray($result);
		
	}
	
	public function removeModule($mod_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->deleteModule($mod_id);
		return $result;
	}
	
	public function createModule($name, $description, $author_id, $category_id, $price) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->insertModule($name, $description, $author_id, $category_id, $price);
		return $result;
		
	}
	
	public function updateModule($id, $name, $description, $category_id, $price) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->updateModule($id, $name, $description, $category_id, $price);
		return $result;
	}
	
	public function getAllCategories() {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listAllCategory();
		return $result;
	}
	
	public function searchModule($pattern) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->retriveModuleItem($pattern);
		return $this->fetchArray($result);
	}
	
	public function buyModule($user_id, $mod_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->addModuleToUser($user_id, $mod_id);
		return $result;
	}
	
	public function publishModule($mod_id){
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->setPublishModule($mod_id);
		return $result;
	}
	
	public function unpublishModule($mod_id){
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->setUnpublishModule($mod_id);
		return $result;
	}
	
	public function isModuleUnique($name) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listModuleByName($name);
		$count = $result->num_rows;
		if($count == 0) {
			return "true";
		}
		return "false";
	}
	
public function isModuleEditUnique($name, $currentName) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listModuleByName($name);
		if($result->num_rows == 1) {
			$moduleName = $this->fetchArray($result);
			if($moduleName[0]['NAME'] != $currentName) {
				return 'false';
			}
		}
		return 'true';
	}
}


?>