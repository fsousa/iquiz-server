<?php
require_once  $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/model/DataBaseAccess.class.php";

class QuestionController {

	static private $instance;

	private function __construct()  {

	}

	public function __clone() {
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}

	static public function getInstance() {
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}

		return self::$instance;
	}

	private function fetchArray($mysqlObject) {
		$rows = array();
		while($r = $mysqlObject->fetch_array(MYSQL_ASSOC)) {
			$rows[] = $r;
		}
		return $rows;
	}
	
	public function getAllQuestionByModule($mod_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listAllQuestionByModule($mod_id);
		return $this->fetchArray($result);
	}
	
	public function getAlternativesByQuestion($quiz_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listAlternativesByQuestion($quiz_id);
		return $this->fetchArray($result);
	}
	
	public function createQuestion($mod_id, $question) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->insertQuestionOnModule($mod_id, $question);
		return $result;
	}
	
	public function getQuizID($mod_id, $question) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listQuizID($mod_id, $question);
		return $this->fetchArray($result);
	}
	
	public function getQuizItemById($quiz_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listQuizById($quiz_id);
		return $this->fetchArray($result);
	}
	
	public function updateQuestion($quiz_id, $question) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->updateQuestionOnModule($quiz_id, $question);
		return $result;
	}
	
	public function removeQuestion($quiz_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->removeQuestionOnModule($quiz_id);
		return $result;
	}
	
	public function getItemAlternativeByID($alternative_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listItemAlternativeByID($alternative_id);
		return $this->fetchArray($result);
	}
	
	public function createQuestionAlternative($quiz_id, $alternative, $correct) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->insertQuizAlternative($quiz_id, $alternative, $correct);
		return $result;
	
	}
	
	public function removeQuestionAlternative($alternative_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->deleteQuestionAlternative($alternative_id);
		return $result;
	}
	
	public function editQuestionAlternative($alternative_id, $alternative, $correct) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->updateQuestionAlternative($alternative_id, $alternative, $correct);
		return $result;
	}
	
	public function isQuestionUniqueOnModule($mod_id, $question) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listQuizID($mod_id, $question);
		$count = $result->num_rows;
		if($count == 0) {
			return "true";
		}
		return "false";
	}
	
	public function isQuestionEditUniqueOnModule($mod_id, $question, $currentQuestion) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listQuizID($mod_id, $question);
		$count = $result->num_rows;
		if($count == 1) {
			$questionValue = $this->fetchArray($result);
			if($questionValue[0]['question'] != $currentQuestion) {
				return 'false';
			}
		}
		return 'true';
	}
	
	public function isAlternativeUniqueOnQuiz($quiz_id, $alternative) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listAlternativeByQuestionAndId($quiz_id, $alternative);
		$count = $result->num_rows;
		if($count == 0) {
			return "true";
		}
		return "false";
	}
	
	public function isAlternativeEditUniqueOnModule($quiz_id, $alternative, $currentAlternative) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listAlternativeByQuestionAndId($quiz_id, $alternative);
		$count = $result->num_rows;
		if($count == 1) {
			$alternativeValue = $this->fetchArray($result);
			if($alternativeValue[0]['alternative'] != $currentAlternative) {
				return 'false';
			}
		}
		return 'true';
	}
}