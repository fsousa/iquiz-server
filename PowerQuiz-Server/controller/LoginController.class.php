<?php
require_once  $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/model/DataBaseAccess.class.php";

class LoginController {
	static private $instance;
	
	private function __construct()  {
	
	}
	
	public function __clone() {
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}
	
	static public function getInstance() {
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}
	
		return self::$instance;
	}
	
	private function fetchArray($mysqlObject) {
		$rows = array();
		while($r = $mysqlObject->fetch_array(MYSQL_ASSOC)) {
			$rows[] = $r;
		}
		return $rows;
	}
	
	public function getLoginCredencials($login, $password) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->loginUser($login, $password);
		$count = $result->num_rows;
		if($count == 1) {
			return $this->fetchArray($result);
		}
		return false;
	}
	
	public function getDataByEmail($email) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->userDataByEmail($email);
		$count = $result->num_rows;
		if($count == 1) {
			return $this->fetchArray($result);
		}
		return false;
	}
	
	public function setRegisterUser($name, $lastName, $email, $password) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->registerUser($name, $lastName, $email, $password);
		return $result;
	}
	
	public function setRegisterUserGoogle($name, $lastName, $email, $password) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->registerUserGoogle($name, $lastName, $email, $password);
		return $result;
	}
	
	public function isUniqueEmail($email) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listUserDetails($email);
		$count = $result->num_rows;
		if($count == 0) {
			return "true";
		}
		return "false";
	}
	
	public function editUser($name, $lastName, $email, $photo) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		if($photo != "") {
			$tmpName = $photo['tmp_name'];
			$fp = fopen($tmpName, 'r');
			$data = fread($fp, filesize($tmpName));
			$data = addslashes($data);
			fclose($fp);
			
			$dataBaseInstance->updateUserPhoto($email, $photo);
		}
		$result = $dataBaseInstance->updateUser($name, $lastName, $email);
		if($result) {
			session_start();
			$_SESSION['name'] = $name;
			$_SESSION['last_name'] = $lastName;
		}
		return $result;
	}
	
	public function updateUserImage($email, $photo) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->updateUserPhoto($email, $photo);
		return $result;
	}
	
	public function deleteUser($email) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->removeUser($email);
		return $result;
	}
}





