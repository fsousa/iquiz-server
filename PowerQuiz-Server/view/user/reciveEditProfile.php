<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/LoginController.class.php";

$name = $_POST['name'];
$lastName = $_POST['last_name'];
$email = $_POST['email'];

$obj3 = LoginController::getInstance();

$result = $obj3->editUser($name, $lastName, $email, "");
if($result) {
	echo '<span class="label label-success">Profile Updated</span>';
} else {
	echo '<span class="label label-important">Something Went Wrong</span>';
}
?>