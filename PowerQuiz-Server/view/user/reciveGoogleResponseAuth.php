<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/google-api-php-client/src/Google_Client.php";
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/google-api-php-client/src/contrib/Google_Oauth2Service.php";
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/LoginController.class.php";

session_start();

 /*//PRODUCE
$client = new Google_Client();
$client->setApplicationName("Power Quiz");
$client->setClientId('563598131953-sjukfmuasdbra4mphcuudcqjm6q43uhv.apps.googleusercontent.com');
$client->setClientSecret('_CdijKgljmSKICK7cPeirij7');
$client->setRedirectUri('http://projeto1-felipe.embedded.ufcg.edu.br/iquiz/PowerQuiz-Server/view/user/reciveGoogleResponseAuth.php');
$client->setDeveloperKey('AIzaSyCpfn6DmQmNNbD7qM1VaApklJF5q5JAUKQ');
$oauth2 = new Google_Oauth2Service($client);
*/ //DEV
$client = new Google_Client();
$client->setApplicationName("Power Quiz");
$client->setClientId('282622232978.apps.googleusercontent.com');
$client->setClientSecret('TXQQJ1ESfB62_zgdL7b7zEJE');
$client->setRedirectUri('http://powerquiz.com/iquiz/PowerQuiz-Server/view/user/reciveGoogleResponseAuth.php');
$client->setDeveloperKey('AIzaSyCbDoi_QYr5OVrz4oo4ysOq6TsfNeUMh2Q');
$oauth2 = new Google_Oauth2Service($client);


if (isset($_GET['code'])) {
	$token = $client->authenticate($_GET['code']);
	if ($client->getAccessToken()) {
		$user = $oauth2->userinfo->get();
		
		$obj3 = LoginController::getInstance();
		$result = $obj3->isUniqueEmail($user['email']);
		
		if($result == "true") {
			
			$obj3->setRegisterUserGoogle($user["given_name"], $user['family_name'], $user['email'], $token);
			$obj3->updateUserImage($user['email'], $user['picture']);
			
			$_SESSION['id'] = $user['id'];
			$_SESSION['login'] = $user['email'];
			$_SESSION['name'] = $user["given_name"];
			$_SESSION['last_name'] = $user['family_name'];
			$_SESSION['image'] = $user['picture'];
			
		}else {
		
			$result = $obj3->getDataByEmail($user['email']);
			
			$name = $result[0]['name'];
			$last_name = $result[0]['last_name'];
			$id = $result[0]['user_id'];
			$image = $result[0]['image'];
			
			$_SESSION['id'] = $id;
			$_SESSION['login'] = $result[0]['email'];
			$_SESSION['name'] = $name;
			$_SESSION['last_name'] = $last_name;
			$_SESSION['image'] = $image;
		
		}
		
		
		header('Location: /iquiz/PowerQuiz-Server/view');
		
	}else {
		session_destroy();
	}
	
	return;
}

?>