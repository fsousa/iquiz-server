<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/LoginController.class.php";

$name = $_POST['name'];
$lastName = $_POST['last_name'];
$email = $_POST['email'];
$password = sha1(sha1($_POST['pass']).$email);

$obj3 = LoginController::getInstance();

$result = $obj3->setRegisterUser($name, $lastName, $email, $password);

if($result) {
	echo 'True';
} else {
	echo '<span class="label label-important">Something Went Wrong</span>';
}