<?php 
session_start();
$email = $_SESSION['login'];
$name = $_SESSION['name'];
$lastName = $_SESSION['last_name'];
$id = $_SESSION['id'];

?>
<h1>Account Manager</h1>
<div class="row-fluid">
	<div class="span2">
		<?php 
		if(!isset($_SESSION['image']) || $_SESSION['image'] == "") { ?>
		<img src="images/default_user.png" class="img-rounded">
		<?php }else {?>
		<img style="max-width: 200px; max-height: 200px; line-height: 20px;"
			src='<?php echo $_SESSION["image"]; ?>' class="img-rounded">
		<?php }
		?>
	</div>
	<div class="span4">
		<form id="showProfile" class="showProfile" onsubmit="return false;">
			<fieldset>
				<h2>
					<?php echo $name ." ". $lastName?>
				</h2>
				<h4>Email:</h4>
				<p>
					<?php echo $email ?><span class="label label-important">Fixed</span>
				</p>

				<button onclick="showPage('user/editProfile.php')" type="submit"
					name="btnEdit" class="btn btn-primary" onclick="">Edit</button>
			</fieldset>
		</form>
	</div>
</div>

