<div class="loginView">
	<div class="container">
		<form class="form-signin" id="form-signup" name="form-signup" onsubmit="return false">
			<div class="MyResult"></div>
			<fieldset>
				<h2 class="form-signin-heading">Register</h2>
				<label>All the fields are required</label>
				<input type="text" class="input-block-level" placeholder="First Name" id="name" name="name">
				<input type="text" class="input-block-level" placeholder="Last Name" id = "last_name" name="last_name">  
				<input type="text" class="input-block-level" placeholder="Email address" id="email" name="email"> 
				<input type="password" class="input-block-level" placeholder="Password" id = "pass" name="pass"> 
				<input type="password" class="input-block-level" placeholder="Password again" id ="pass2" name="pass2"> 
				<label class="checkbox"> <input type="checkbox" value="remember-me">
					I accept the terms of service.
				</label>
				<button onclick="validUserRegister();"
					class="btn btn-large btn-primary" type="submit">Send</button>
			</fieldset>
		</form>
	</div>
</div>