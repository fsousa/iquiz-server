<?php
session_start();
$email = $_SESSION['login'];
$name = $_SESSION['name'];
$lastName = $_SESSION['last_name'];
$image = $_SESSION['image'];

?>

<h1>Edit Profile</h1>
<br>
<div class="row-fluid">
	<form id="editProfileForm" class="editProfileForm"
		onsubmit="return false;" enctype="multipart/formdata">
		<input type="hidden" id="email" name="email"
			value="<?php echo $email;?>">
		<div class="MyResult"></div>
		<fieldset>
			<label for="name">First Name</label> <input id="name" name="name"
				type="text" value="<?php echo $name ?>" placeholder=""> <label
				for="last_name">Last Name</label> <input id="last_name"
				name="last_name" type="text" value="<?php echo $lastName ?>"
				placeholder="">
				<label>Photo</label>
				<input type="file" name="myimage" id="myimage"/>
				<br>

			<button type="submit" name="btnUpdate" class="btn btn-primary"
				onclick="validateUpdateProfile()">Update</button>
			<button type="submit" name="btnDelete" class="btn btn-danger"
				onclick="deleteAccount()">Delete</button>
		</fieldset>


	</form>
	<small> <a onclick="showPage('user/index.php')" href="#"> BACK </a>
</small>
</div>
