<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/LoginController.class.php";
session_start();
$email = $_SESSION['login'];

$obj3 = LoginController::getInstance();

$result = $obj3->deleteUser($email);

if($result) {
	echo 'True';
} else {
	echo '<span class="label label-important">Something Went Wrong</span>';
}
?>