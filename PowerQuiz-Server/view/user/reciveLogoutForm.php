<?php

session_start();
unset($_SESSION['id']);
unset($_SESSION['login']);
unset($_SESSION['name']);
unset($_SESSION['last_name']);
unset($_SESSION['image']);
session_destroy();
