<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/LoginController.class.php";
$login = $_POST['email'];
$password = sha1(sha1($_POST['pass']).$login);

$obj3 = LoginController::getInstance();

$result = $obj3->getLoginCredencials($login, $password);

if($result) {
	$name = $result[0]['name'];
	$last_name = $result[0]['last_name'];
	$id = $result[0]['user_id'];
	$image = $result[0]['image'];
	
	if(!isset($_SESSION)) {
		session_start();
		$_SESSION['id'] = $id;
		$_SESSION['login'] = $login;
		$_SESSION['name'] = $name;
		$_SESSION['last_name'] = $last_name;
		$_SESSION['image'] = $image;
		echo "True";
	}
}else {
	echo '<span class="label label-important">Username or password do not match</span>';
}

?>
