<?php
require $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/QuestionController.class.php";

$quiz_id = $_POST['quiz_id'];
$question = $_POST['question'];

$obj2 = QuestionController::getInstance();
$result = $obj2->updateQuestion($quiz_id, $question);

if($result) {
	echo '<span class="label label-success">Quiz Edit Successfully</span>';
}else {
	echo '<span class="label label-important">Something Went Wrong</span>';
}

?>