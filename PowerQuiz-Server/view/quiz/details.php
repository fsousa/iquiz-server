<?php 
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/QuestionController.class.php";
$quiz_id = $_GET['qid'];

$obj2 = QuestionController::getInstance();
$quiz = $obj2->getQuizItemById($quiz_id);
$alternatives = $obj2->getAlternativesByQuestion($quiz_id);

$mId = $quiz[0]['mod_id'];
$question = $quiz[0]['question'];


?>

<h1>Question Details</h1>

<div class="row-fluid">
	<div class="span4">
		<h3>Question:</h3>
		<p>
			<?php echo $question; ?>
		</p>
		<button type="submit" name="btnAdd" class="btn btn-primary"
			onclick="showPage('quiz/edit.php?q_id=<?php echo $quiz_id;?>')">Edit</button>

	</div>
	<div class="span8">
		<h3>Alternatives:</h3>
		<?php 
		if(sizeof($alternatives) == 0) {

				echo "No alternatives.<br>";
			}else {
			?>
		<ul>
			<?php 
			foreach ($alternatives as &$value) {
					if($value['correct'] == 1) {?>
			<li><a href="#"
				onclick="showPage('quiz/editAlternative.php?qid=<?php echo $quiz_id?>&alternative_id=<?php echo $value['alternative_id']?>')"><?php echo  $value["alternative"]?>
			</a>&nbsp;<span class="label label-success">Correct</span></li>
			<?php } else {?>
			<li><a href="#"
				onclick="showPage('quiz/editAlternative.php?qid=<?php echo $quiz_id?>&alternative_id=<?php echo $value['alternative_id']?>')"><?php echo  $value["alternative"]?>
			</a></li>
			<?php	}
				 }
			}
			?>
		</ul>
		<button type="submit" name="btnAdd" class="btn btn-primary"
			onclick="showPage('quiz/createAlternative.php?qid=<?php echo $quiz_id?>')">Add</button>
	</div>
</div>
<br>
<small> <a onclick="showPage('module/details.php?id=<?php echo $mId?>')"
	href="#"> BACK </a>
</small>
