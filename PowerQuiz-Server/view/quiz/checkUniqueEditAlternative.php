<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/QuestionController.class.php";
$quiz_id = $_POST['quiz_id'];
$alternative = $_POST['alternative'];
$currentAlternative = $_POST['currentAlternative'];
$obj2 = QuestionController::getInstance();

$result = $obj2->isAlternativeEditUniqueOnModule($quiz_id, $alternative, $currentAlternative);

echo $result;

?>
