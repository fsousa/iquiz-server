<?php 
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/QuestionController.class.php";

$quiz_id = $_GET['q_id'];

$obj2 = QuestionController::getInstance();
$quiz = $obj2->getQuizItemById($quiz_id);

$question = $quiz[0]['question'];
$mId = $quiz[0]['mod_id'];
?>

<h1>Edit Question</h1>
<br>
<form id="editquestion" name="editquestion" onsubmit="return false" accept-charset="iso-8859-1">
	<div class="MyResult"></div>
	<fieldset>
		<legend>All fields are required</legend>
		<input type="hidden" id="mod_id" name="mod_id" value="<?php echo $mId; ?>">
		<input type="hidden" id="quiz_id" name="quiz_id" value="<?php echo $quiz_id; ?>">
		<input type="hidden" id="currentQuestion" name="currentQuestion" value="<?php echo $question; ?>">
		<label>Question</label>
		<textarea rows="4" cols="50" 
			placeholder="" id="question" name="question"><?php echo $question;?></textarea><br>
		
		<button type="submit" name="btnSend" class=" btn btn-primary" onclick="validEditQuestion()">Update</button>
		<button type="submit" name="btnDelete" class="btn btn-danger" onclick="return submitFormQuizDelete();">Delete</button>
	</fieldset>
</form>
<small> <a onclick="showPage('quiz/details.php?qid=<?php echo $quiz_id?>')" href="#"> BACK </a>
</small>
