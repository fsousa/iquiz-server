<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/QuestionController.class.php";
$question = $_POST['question'];
$mod_id = $_POST['mod_id'];
$currentQuestion = $_POST['currentQuestion'];
$obj2 = QuestionController::getInstance();

$result = $obj2->isQuestionEditUniqueOnModule($mod_id, $question, $currentQuestion);

echo $result;