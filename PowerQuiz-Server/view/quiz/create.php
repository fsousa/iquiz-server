<?php $mId = $_GET['id'];?>

<h1>Create a New Quiz</h1>
<div class="row-fluid">
	<div class="span8">
		<form id="createquiz" name="createquiz" onsubmit="return false;">
			<div class="MyResult"></div>
			<fieldset>
				<input type="hidden" id="mod_id" name="mod_id"
					value="<?php echo $mId?>">
				<legend>All fields are required</legend>
				<label>Question</label>
				<textarea id="question" name="question" rows="4" cols="50"
					placeholder=""></textarea>
				<div id="dynamicInput">
					Alternative 1<br>
					<textarea rows="4" cols="50" name="myInputs[]"></textarea>
					<label class='radio inline'> <input checked="checked"
						name='myInputs2' type='radio' value=""> <span
						class="label label-success">Correct</span>
					</label>
				</div>
				<input class="btn btn-warning" type="button"
					value="Add another input" onClick="addInput('dynamicInput');">
				<button onclick="validCreateQuestion()" type="submit"
					class="btn btn-primary">Submit</button>
			</fieldset>
		</form>
		<small> <a
			onclick="counter=1; showPage('module/details.php?id=<?php echo $mId?>')"
			href="#"> BACK </a>
		</small>
	</div>
</div>
