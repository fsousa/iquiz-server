<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/QuestionController.class.php";

$question = $_POST['question'];
$mod_id = $_POST['mod_id'];
$myInputs = $_POST["myInputs"];
$myInputs2 = $_POST["myInputs2"];


$obj2 = QuestionController::getInstance();
$result = $obj2->createQuestion($mod_id, $question);

$q_id = $obj2->getQuizID($mod_id, $question);
for ($i=0; $i < sizeof($myInputs); $i++) {
	if($i == $myInputs2 -1) {
		$result = $obj2->createQuestionAlternative($q_id[0]['quiz_id'],  $myInputs[$i], 1);
	} else {
		$result = $obj2->createQuestionAlternative($q_id[0]['quiz_id'],  $myInputs[$i], 0);
	}
	
}


if($result) {
	echo '<span class="label label-success">Question Create Successfully</span>';
}else {
	echo '<span class="label label-important">Something went wrong</span>';
}



