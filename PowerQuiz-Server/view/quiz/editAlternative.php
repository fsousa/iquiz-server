<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/QuestionController.class.php";
$quiz_id = $_GET['qid'];
$alternative_id = $_GET['alternative_id'];

$obj2 = QuestionController::getInstance();
$alter = $obj2->getItemAlternativeByID($alternative_id);

?>
<h1>Edit Alternative</h1>

<div class="container">
	<div class="row-fluid">
		<div class="span4">
			<form id="EditAlternative" name="EditAlternative"
				onsubmit="return false;">
				<div class="MyResult"></div>
				<fieldset>
					<input type="hidden" id ="alternative_id" name="alternative_id" value="<?php echo $alternative_id;?>">
					<input type="hidden" id="quiz_id" name="quiz_id" value="<?php echo $quiz_id;?>">
					<input type="hidden" id="currentAlternative" name="currentAlternative" value="<?php echo $alter[0]['alternative']?>">
					<legend>All fields are required</legend>
					<label>Alternative</label><br>
					<textarea rows="4" cols="50" id="alternative" name="alternative"><?php echo $alter[0]['alternative']?></textarea>
					<label class='checkbox inline'> <input <?php if($alter[0]['correct'] == "1") {echo 'checked="checked"';}?> name='correct' type='checkbox'
						value="1"> <span class="label label-success">Correct</span>
					</label>

					<button onclick="validEditAlternative()" type="submit" class="btn btn-primary">Update</button>
					<button onclick="return submitFormAlternativeDelete()" type="submit" class="btn btn-danger">Delete</button>
				</fieldset>
			</form>
			<small> <a
				onclick="counter=1; showPage('quiz/details.php?qid=<?php echo $quiz_id ?>')"
				href="#"> BACK </a>
			</small>
		</div>
	</div>
</div>
