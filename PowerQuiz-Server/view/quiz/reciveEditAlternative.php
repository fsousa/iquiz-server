<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/QuestionController.class.php";

$alternative_id = $_POST['alternative_id'];
$alternative = $_POST['alternative'];
$correct = $_POST['correct'];

$obj2 = QuestionController::getInstance();
$result = $obj2->editQuestionAlternative($alternative_id, $alternative, $correct);

if($result) {
	echo '<span class="label label-success">Alternative Update Successfully</span>';
}else {
	echo '<span class="label label-important">Something went wrong</span>';
}

