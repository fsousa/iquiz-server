<?php 

$quiz_id = $_GET['qid'];

?>
<h1>Create a New Alternative</h1>

<div class="container">
	<div class="row-fluid">
		<div class="span4">
			<form id="createAlternative" name="createAlternative"
				onsubmit="return false;">
				<div class="MyResult"></div>
				<fieldset>
					<input type="hidden" id="quiz_id" name="quiz_id" value="<?php echo $quiz_id;?>">
					<legend>All fields are required</legend>
					<label>Alternative</label><br>
					<textarea rows="4" cols="50" id="alternative" name="alternative"></textarea>
					<label class='checkbox inline'> <input name='correct' type='checkbox'
						value="1"> <span class="label label-success">Correct</span>
					</label>

					<button onclick="validCreateAlternative()" type="submit" class="btn btn-primary">Submit</button>
				</fieldset>
			</form>
			<small> <a
				onclick="showPage('quiz/details.php?qid=<?php echo $quiz_id ?>')"
				href="#"> BACK </a>
			</small>
		</div>
	</div>
</div>
