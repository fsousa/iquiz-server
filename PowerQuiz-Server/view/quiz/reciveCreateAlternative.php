<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/QuestionController.class.php";

$quiz_id = $_POST['quiz_id'];
$alternative = $_POST['alternative'];
$correct = $_POST['correct'];

$obj2 = QuestionController::getInstance();
if($correct != 1) { 
	$correct = 0;
}
$result = $obj2->createQuestionAlternative($quiz_id,  $alternative, $correct);

if($result) {
	echo '<span class="label label-success">Alternative Create Successfully</span>';
}else {
	echo '<span class="label label-important">Something went wrong</span>';
}