<?php 
session_start();
?>
<!DOCTYPE html>

<html lang="en">
<head>

<title>iQuiz</title>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="../bootstrapExtended/css/bootstrap.css">
<link rel="stylesheet" href="../bootstrapExtended/css/bootstrap-fileupload.min.css">
<link rel="stylesheet" href="../css/showModule.css">
<link rel="stylesheet" href="../css/login.css">
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="center-menu">
				<a class="brand" href="#">iQuiz</a>
				<div class="nav-collapse collapse">
					<ul class="nav">
						<li onclick="showPage('welcome.php')" class="active"><a href="#">Home</a>
						</li>
						<?php 
							if(isset($_SESSION['name'])) {
						?>
						<li><a onclick="showPage('module/index.php')" href="#mManager">Module</a>
						</li>
						<li><a onclick="showPage('user/index.php')" href="#aManager">Account</a>
						</li>
						<?php }?>
						<li><a onclick="showPage('about/index.php')" href="#about">About</a></li>
						<li><a onclick="showPage('contact/index.php')" href="#contact">Contact</a>
						</li>
					</ul>
					<div class="loginViewText">
						<?php 
							if(!isset($_SESSION['image']) || $_SESSION['image'] == "") { ?>
								<img src="images/default_user.png" style="display: inline; margin-top: -4px" width="32" height="32"  class="img-rounded">"
							<?php }else {?>
								<img src='<?php echo $_SESSION["image"]; ?>' style="display: inline; margin-top: -4px" width="32" height="32"  class="img-rounded">"
							<?php }
						?>
						<label>Hello,
						<?php
							if(!isset($_SESSION['name'])) {
								echo "Visitant";
								?></label> <a onclick="showPage('user/login.php')" href="#login">Sign In</a><?php 
							} else {
								echo $_SESSION['name'];?>
								</label> <a onclick="logout()" href="#login">Logout</a><?php 
							}
						?>
						
					</div>
				</div>
			</div>
			<!--/.nav-collapse -->
		</div>
		<div class="container" id="txtHint">
			<?php include_once 'welcome.php';?>
		</div>
	</div>
	
	<div class="container">
		<div class="navbar navbar-inverse navbar-fixed-bottom">
			<div class="navbar-inner">
				<footer class="footer">Powered by Fsousa.pro</footer>
			</div>
		</div>
	</div>
	
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="../js/jquery.js"></script>
	<script src="../js/jquery.form.js"></script>
	<script src="../js/dimanicField.js"></script>
	<script src="../js/packageModule.js"></script>
	<script src="../js/jquery.validate.js"></script>
	<script src="../js/formsValidate.js"></script>
	<script src="../bootstrapExtended/js/bootstrap-fileupload.min.js"></script>
</body>
</html>
