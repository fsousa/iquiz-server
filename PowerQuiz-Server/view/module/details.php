<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/ModuleController.class.php";
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/QuestionController.class.php";
$id = $_GET['id'];

$obj1 = ModuleController::getInstance();
$obj2 = QuestionController::getInstance();

$moduleEdit = $obj1->getModuleByID($id);
$categories = $obj1->getAllCategories();

while($item = $moduleEdit->fetch_array()) {
	$mId = $item['ID'];
	$name = $item['NAME'];
	$description = $item['DESCRIPTION'];
	$category = $item['CATEGORY'];
	$price = $item['PRICE'];
	$publish = $item['PUBLISH'];
}

$questions = $obj2->getAllQuestionByModule($mId);
?>

<h1>Module Details</h1>

<div class="row-fluid">
	<div class="span4">
		<form id="publishModule" class="publishModule"
			onsubmit="return false;">
			<fieldset>
				<h2>
					<?php echo $name?>
				</h2>
				<h4>Description:</h4>
				<p>
					<?php echo $description ?>
				</p>

				<h4>Category</h4>
				<p>
					<?php echo $category?>
				</p>

				<h4>Price</h4>
				<p>
					<?php echo $price?>
				</p>
				<input type="hidden" name="mod_id" value="<?php echo $mId?>">
				<button type="submit" name="btnEdit" class="btn btn-primary"
					onclick="return showPage('module/edit.php?id=<?php echo $mId?>'); return false;">Edit</button>
				<?php 
				if($publish == 0) {
					echo '<button type="submit" name="btnPublish" class="btn btn-success" onclick="return submitModulePublish();">Publish</button>';
				} else {
					echo '<button type="submit" name="btnUnpublish" class="btn btn-danger" onclick="return submitModuleUnpublish();">Unpublish</button>';
				}
				?>
			</fieldset>
		</form>
	</div>
	<div class="span8">
		<h2>Questions</h2>
		<?php 
		if(sizeof($questions) == 0) {
				echo "No questions<br>";
			}else {
			?>
		<ul>
			<?php 
			foreach ($questions as &$value) {
				?>
			<li><a href="#"
				onclick="showPage('quiz/details.php?id=<?php echo $mId?>&qid=<?php echo $value['quiz_id'] ?>&q=<?php echo $value['question'] ?>')"><?php echo $value['question'];  ?>
			</a></li>
			<?php 
				}
			}?>
		</ul>
		<button type="submit" name="btnAdd" class="btn btn-primary"
			onclick="showPage('quiz/create.php?id=<?php echo $mId?>')">Add</button>
	</div>
</div>
<br>
<small> <a onclick="showPage('module/show.php')" href="#"> BACK </a>
</small>
