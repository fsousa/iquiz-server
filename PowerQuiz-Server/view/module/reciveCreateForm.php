<?php
require $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/ModuleController.class.php";
session_start();
$name = $_POST['name'];
$description = $_POST['textArea'];
$author_id  = $_SESSION['id'];
$category_id = $_POST['categorys'];
$price = $_POST['price'];


$obj1 = ModuleController::getInstance();
$result = $obj1->createModule($name, $description, $author_id, $category_id, $price);
if($result) {
	echo '<span class="label label-success">Module Create Successfully</span>';
}else {
	echo '<span class="label label-important">Something went wrong</span>';
}
?>