<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/ModuleController.class.php";
$id = $_GET['id'];

$obj1 = ModuleController::getInstance();

$moduleEdit = $obj1->getModuleByID($id);
$categories = $obj1->getAllCategories();

while($item = $moduleEdit->fetch_array()) {
	$mId = $item['ID'];
	$name = $item['NAME'];
	$description = $item['DESCRIPTION'];
	$category = $item['CATEGORY'];
	$price = $item['PRICE'];
}

?>

<h1>Edit Module</h1>
<br>
<form id="editmodule" name="editmodule" onsubmit="return false">
	<div class="MyResult"></div>
	<fieldset>
		<legend>All fields are required</legend>
		<input type="hidden" name="id" value="<?php echo $mId;?>">
		<input type="hidden" id="currentName" name="currentName" value="<?php echo $name;?>">
		<label for="name">Module Name</label> <input id="name" name="name" type="text" value="<?php echo $name ?>" placeholder=""> 
		<label for="description">Description</label>
		<textarea rows="4" cols="50" 
			placeholder="" name="description" id="description"><?php echo $description;?></textarea>
		<label>Category</label> <select id="category"  name="category">
			<option value="">Select a Category:</option>
			<?php
			while($row = $categories->fetch_array()) {
				if($row['name'] == $category) {
					echo "<option selected='selected' value=".$row["cat_id"].">".$row['name']."</option>";
				} else {
					echo "<option value=".$row["cat_id"].">".$row['name']."</option>";
				}
				?>
			<?php }?>
		</select><br> <label for="price">Price</label> <input id="price" name="price" value="<?php echo $price ?>"  type="text"
			placeholder="0 for FREE"><br>
		<button type="submit" name="btnSend" class=" btn btn-primary" onclick="validEditModule()">Update</button>
		<button type="submit" name="btnDelete" class="btn btn-danger" onclick="return submitFormModuleDelete()">Delete</button>
	</fieldset>
</form>
<small> <a onclick="showPage('module/details.php?id=<?php echo $mId?>')" href="#"> BACK </a>
</small>
