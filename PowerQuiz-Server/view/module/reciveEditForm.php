<?php
require $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/ModuleController.class.php";

$id = $_POST['id'];
$name = $_POST['name'];
$description = $_POST['description'];
$category_id = $_POST['category'];
$price = $_POST['price'];


$obj1 = ModuleController::getInstance();
$result = $obj1->updateModule($id, $name, $description, $category_id, $price);
if($result) {
	echo '<span class="label label-success">Module Edit Successfully</span>';
}else {
	echo '<span class="label label-important">Something Went Wrong</span>';
}
?>