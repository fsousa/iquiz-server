<table class="table table-striped" id="searchResult">
<th>NAME</th>
<th>DESCRIPTION</th>
<th>CATEGORY</th>
<th>PRICE</th>
<th>PUBLISH</th>
<?php

foreach ($modules as &$value) {
	echo '<tr>';
	echo '<td>';?>
	<a onclick="showPage('module/details?id=<?php echo $value['ID']; ?>')"
		href="#module/edit"> <?php echo $value['NAME']?>
	</a>
	</td>
	<?php echo '<td>' . $value['DESCRIPTION'] . '</td>';
	echo '<td>'.$value['CATEGORY'].'</td>';
	echo '<td>'.$value['PRICE'].'</td>';
	if($value['PUBLISH'] == 1) {
		echo '<td><span class="badge badge-success">Yes</span></td>';
	}else {
		echo '<td><span class="badge badge-important">No</span></td>';
	}
	echo '</tr>';
}

?>

</table>