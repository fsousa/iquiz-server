<?php 
require $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/ModuleController.class.php";

$obj1 = ModuleController::getInstance();

$categories = $obj1->getAllCategories();
?>
<h1>Create a New Module</h1>
<br>
	<form id="createmodule" name="createmodule" onsubmit="return false;" accept-charset="iso-8859-1">
		<div class="MyResult"></div>
		<fieldset>
			<legend>All fields are required</legend>
			<label for="name">Module Name</label> <input id="name" name="name" type="text"
				placeholder=""> <label>Description</label>
			<textarea name="textArea" rows="4" cols="50" placeholder=""></textarea>
			<label>Category</label> <select id ="categorys" name="categorys">
				<option value="">Select a Category:</option>
				<?php
				while($row = $categories->fetch_array()) {
				echo "<option value=".$row["cat_id"].">".$row['name']."</option>";
				?>
				<?php }?>
			</select><br> <label for="price">Price</label> <input id="price" name="price" type="text"
				placeholder="0 for FREE"><br>

			<button onclick="validCreateModule()" type="submit" class="btn btn-primary">Submit</button>
		</fieldset>
	</form>
<small> <a onclick="showPage('module/index.php')" href="#"> BACK </a>
</small>
