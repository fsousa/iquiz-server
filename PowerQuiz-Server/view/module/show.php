<?php 
require $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/ModuleController.class.php";
session_start();
$obj1 = ModuleController::getInstance();

$modules = $obj1->getAllModulesByUser($_SESSION['login']);
?>

<div class="leftSearch">
	<form class="form-search">
		<input type="text" placeholder="Search"
			class="input-medium search-query" onkeyup="searchModule(this.value)">
	</form>

</div>

<div>
	<h3>Your modules</h3>
</div>

<?php include "showTable.php";?>
<small> <a onclick="showPage('module/index.php')" href="#"> BACK </a>
</small>
