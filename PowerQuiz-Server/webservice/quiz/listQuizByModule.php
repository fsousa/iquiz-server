<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/QuestionController.class.php";

$mod_id = $_GET['mod_id'];

$obj2 = QuestionController::getInstance();
$result = $obj2->getAllQuestionByModule($mod_id);

header('Content-type: application/json charset=UTF-8');
echo json_encode($result);