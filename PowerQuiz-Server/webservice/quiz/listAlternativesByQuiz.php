<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/QuestionController.class.php";
$obj2 = QuestionController::getInstance();

$quiz_id = $_GET['quiz_id'];
$result = $obj2->getAlternativesByQuestion($quiz_id);

header('Content-type: application/json charset=UTF-8');
echo json_encode($result);