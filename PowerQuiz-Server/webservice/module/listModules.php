<?php
require $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/ModuleController.class.php";

$obj1 = ModuleController::getInstance();

$result = $obj1->getAllModules();
header('Content-type: application/json charset=UTF-8');
echo json_encode($result);

?>