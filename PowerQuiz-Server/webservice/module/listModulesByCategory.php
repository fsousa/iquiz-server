<?php
require $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/ModuleController.class.php";


$cat_name = $_GET['cat_name'];

$obj1 = ModuleController::getInstance();
$result = $obj1->getAllModulesByCategory($cat_name);
header('Content-type: application/json charset=UTF-8');
echo json_encode($result);

?>