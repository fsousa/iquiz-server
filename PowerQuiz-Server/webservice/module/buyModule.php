<?php
require $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/ModuleController.class.php";

$mod_id = $_GET['mod_id'];
$user_id = $_GET['user_id'];

$obj1 = ModuleController::getInstance();
$result = $obj1->buyModule($mod_id, $user_id);
header('Content-type: application/json charset=UTF-8');
echo json_encode($result);

?>