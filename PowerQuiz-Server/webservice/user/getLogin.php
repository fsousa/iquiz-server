<?php
require_once $_SERVER['DOCUMENT_ROOT']."/iquiz/PowerQuiz-Server/controller/LoginController.class.php";

$login = $_GET['email'];
$password = $_GET['pass'];

$obj3 = LoginController::getInstance();

$result = $obj3->getLoginCredencials($login, $password);

if($result) {
	echo json_encode($result);
} else {
	echo "false";
}
